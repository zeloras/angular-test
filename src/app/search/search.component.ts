import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent {
    
    private inputQuery;
    private HttpClient;
    private apiKey = '26f3e70b';
    private apiUrl = 'https://www.omdbapi.com';
    public moviewList = [];
    public addedMovies = [];
    public filmControl;

    constructor(private http: HttpClient) {
        this.filmControl = new FormControl();
        this.HttpClient = this.http;
    }

    /**
     * Input keyup event for autocomplete
     * @argument {object} event  
     */
    OnKeyup(event: any) {
        this.inputQuery = event.target.value;
        if (this.inputQuery.length > 2) {
            this.getArrayForAutocomplete(this.inputQuery);
        }
    }

    /**
     * Action after click on autocomplete
     * @argument {string} imdbid 
     */
    selectAutocomplete(imdbid: string) {
        this.addMovie(imdbid);
    }

    /**
     * Get array data for autocomplete list
     * @argument {string} value 
     * @argument {object} http 
     */
    getArrayForAutocomplete(value: string) {
        let $self = this;
        this.HttpClient .get($self.apiUrl + '/?s=' + value + '&apikey=' + $self.apiKey).subscribe(data => {
            if (data.totalResults > 0) {
                $self.moviewList = data['Search'];
            }
        });
    }

    /**
     * Add movie to array 
     * @argument {string} value 
     */
    addMovie (value: string) {
        let $self = this;
        this.HttpClient .get($self.apiUrl + '/?i=' + value + '&apikey=' + $self.apiKey).subscribe(data => {
            if (data.Response) {
                $self.addedMovies.push(data);
            }
        });
    }
    
    /**
     * Remove movie from array
     * @argument {string} value 
     */
    removeMovie (value: number) {
        this.addedMovies.splice(value, 1);
    }
}

